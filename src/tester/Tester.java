package tester;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Map;
import java.util.Set;

import org.junit.Test;

import tema4.Account;
import tema4.Bank;
import tema4.Person;

public class Tester {

	private static final double DELTA = 1e-15;
	
	public void testCreatePerson(){
		Bank bank = new Bank();
		Person person = new Person("Steve", 1);
		bank.createPerson("Steve", 100);
		Map<Integer, Person> persons = bank.getPersonMap();
		Map<Integer, Set<Account>> accounts = bank.getBankMap();
		assertEquals("Steve", persons.get(1).getName());
	}
	
	public void testCreateAccount(){
		Bank bank = new Bank();
		Person person = new Person("Steve", 1);
		bank.createPerson("Steve", 100);
		Map<Integer, Person> persons = bank.getPersonMap();
		Map<Integer, Set<Account>> accounts = bank.getBankMap();
		for(Account account: accounts.get(1)){
			if (account.getId() == 1){
				assertEquals(100.0, account.getSum(), DELTA);
			}
		}
	}
	
	
	@Test
	public void test() {
		testCreatePerson();
		testCreateAccount();
	}

}
