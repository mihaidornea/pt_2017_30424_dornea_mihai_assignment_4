package tema4;

import java.io.Serializable;
import java.util.Observer;
import java.util.Observable;


public class Person implements Serializable, Observer{

	private static final long serialVersionUID = 576019041345429120L;
	private String name; 
	private int id;
	
	public Person(String name, int id){
		this.name = name;
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String toString(){
		return new String("Name: " + this.name + " with id: " + this.id);
	}
	
	public boolean equals(Person person){ 
		if (this == person)
			return true;
		if(getClass() != person.getClass())
			return false;
		Person cast = (Person) person;
		if(!name.equals(cast.name))
			return false;
		if(id != cast.id)
			return false;
		return true;
	}
	
	
	public int hashCode() {
		int hash = 1;
		hash =  hash * 22 + this.id;
		hash = hash *15 + name.hashCode();
		return hash;
	}
	
	@Override
	public void update(Observable account, Object arg1) {
		if(account instanceof Account){
			Account casted = (Account) account;
			System.out.println("Obesrver: " + casted.getId() + " " +arg1.toString());
		}
	}

}
