package tema4;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public abstract class Account extends Observable implements Serializable{
	
	private static final long serialVersionUID = 4538911020549384323L;
	private int id;
	private double sum;
	
	public Account(int id, double sum){
		this.id = id;
		this.sum = sum;
	}
	
	public abstract void deposit(double sum);
	public abstract void withdraw(double sum);
	
	
	
	public int getId(){
		return this.id;
	}
	
	public double getSum(){
		return this.sum;
	}
	
	public void setSum(double sum){
		this.sum = sum;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int hashCode(){
		int hash = 1;
		hash = 15 * this.id;
		return hash;
	}
	
	public boolean equals(Account account){
		if (this.id != account.id)
			return false;
		return true;
	}
	
	public String toString(){
		return new String("Account id: " + this.id +" containing: " + this.sum + " dollars");
	}
	
}
