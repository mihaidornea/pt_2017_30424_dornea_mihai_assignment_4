package tema4;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import gui.ErrorMsg;
public class Bank implements BankProc, Serializable{

	private static final long serialVersionUID = -2826041507998269612L;
	private Map<Integer, Set<Account>> bank;
	private Map<Integer, Person> persons;
	private static int idP;
	private static int idA;
	private ErrorMsg error;
	
	public Bank() { 
		this.bank = new HashMap<Integer, Set<Account>>();
		this.persons = new HashMap<Integer, Person>();
		Bank.idP = 1;
		Bank.idA = 1;
	}
		
	@Override
	public void createPerson(String name, double sum) {
		assert isWellFormed(): error.start("Bank is corrupted");
		Person person = new Person(name, idP);
		Account account = new SpendingAccount(idA, sum);
		Set<Account> accounts = new HashSet<Account>();
		accounts.add(account);
		bank.put(idP, accounts);
		persons.put(idP, person);
		account.addObserver(persons.get(idP));
		idP++;
		idA++;
		assert isWellFormed(): error.start("Bank is corrupted");;
	}

	@Override
	public void createSavingAccount(Person person, double sum) {
		assert isWellFormed(): error.start("Bank is corrupted");
		assert person != null: error.start("You need an actual person for this operation"); 
		assert sum >= 0: error.start("Invalid sum entered");
		if(bank.containsKey(person.getId())){
			int size = bank.get(person.getId()).size();
			SavingAccount savingAccount = new SavingAccount(idA, sum);
			bank.get(person.getId()).add(savingAccount);
			savingAccount.addObserver(persons.get(person.getId()));
			idA++;
			int sizeAfterInsertion = bank.get(person.getId()).size();
			assert sizeAfterInsertion == size+1: error.start("Something went wrong when adding the account");
			assert isWellFormed(): error.start("Bank is corrupted");
		}
		else System.out.println("The person si not registered in the bank");
	}
	
	@Override
	public void createSpendingAccount(Person person, double sum){	
		assert isWellFormed(): error.start("Bank is corrupted");
		assert person != null: error.start("You need an actual person for this operation"); 
		assert sum >= 0: error.start("The sum must be a valid one !");
		if(bank.containsKey(person.getId())){
			int size = bank.get(person.getId()).size();
			SpendingAccount spendingAccount = new SpendingAccount(idA, sum);
			bank.get(person.getId()).add(spendingAccount);
			spendingAccount.addObserver(persons.get(person.getId()));
			idA++;
			int sizeAfterInsertion = bank.get(person.getId()).size();
			assert sizeAfterInsertion == size+1: error.start("Something went wrong when adding the account");
			assert isWellFormed(): error.start("Bank is corrupted");
		}
		else error.start("The person si not registered in the bank");
	}

	public void printit(){
		for (Integer id : bank.keySet()){
			for(Account account : bank.get(id)){
				System.out.println(persons.get(id).toString() + " " + account.toString());
			}
		}
	}
	
	@Override
	public void deposit(Person person, int accountId, double sum) {
		assert isWellFormed(): error.start("Bank is corrupted");
		assert person != null: error.start("You need an actual person for this operation"); 
		assert sum >= 0: error.start("The sum must be a valid one !");
		if(bank.containsKey(person.getId())){
			Set<Account> accounts = bank.get(person.getId());
			for(Account account: accounts){
				if (account.getId() == accountId){
					double initialSum = account.getSum();
					account.deposit(sum);
					double finalSum = account.getSum();
					assert finalSum == initialSum + sum: error.start("Money was not deposited correctly");
				}
			}
		}
		assert isWellFormed(): error.start("Bank is corrupted");
	}

	@Override
	public void withdraw(Person person, int accountId, double sum) throws Exception {
		assert isWellFormed(): error.start("Bank is corrupted");
		assert person != null: error.start("You need an actual person for this operation"); 
		assert sum >= 0: error.start("The sum must be a valid one !");
		if(bank.containsKey(person.getId())){
			Set<Account> accounts = bank.get(person.getId());
			for(Account account: accounts){
				if (account.getId() == accountId){
					double initialSum = account.getSum();
					if (sum > account.getSum()){
						error.start("There is not enough money in the account");
						throw new Exception();
					}
					account.withdraw(sum);
					double finalSum = account.getSum();
					assert finalSum == initialSum + sum: error.start("Money was not withdrawn correctly");				}
			}
		}
		assert isWellFormed(): "Bank is corrupted";
	}

	@Override
	public void deleteAccount(Person person, int accountId) {
		assert isWellFormed(): error.start("Bank is corrupted");
		assert person != null: error.start("You need an actual person for this operation"); 
		if(bank.containsKey(person.getId())){
			Set<Account> accounts = bank.get(person.getId());
			int size = bank.get(person.getId()).size();
			for(Account account: accounts){
				if(account.getId() == accountId){
					accounts.remove(account);
				}
			}
			int sizeAfterInsertion = bank.get(person.getId()).size();
			assert sizeAfterInsertion == size+1: error.start("Something went wrong when adding the account");
		}
		assert isWellFormed(): error.start("Bank is corrupted");
	}

	@Override
	public void deletePerson(Person person) {
		assert isWellFormed(): error.start("Bank is corrupted");
		assert person != null: error.start("You need an actual person for this operation"); 
		if(bank.containsKey(person.getId())){
			bank.remove(person.getId());
			persons.remove(person.getId());
		}
		assert isWellFormed(): error.start("Bank is corrupted");
	}

	@Override
	public Set<Account> listAccounts(Person person) {
			if(bank.containsKey(person.getId()))
				return bank.get(person.getId());
			return null;
	}

	@Override
	public List<Person> listPersons() {
			List<Person> personList = new ArrayList<Person>();
			for(Person person: persons.values()){
				personList.add(person);
			}
		return null;
	}
	
	@Override
	public String checkBalance(Person person, int accountId) {
		if(bank.containsKey(person.getId())){
			Set<Account> accounts = bank.get(person.getId());
			for(Account account: accounts){
				if (account.getId() == accountId){
					return new String("This account has " + account.getSum() + " dollars");
				}
			}
		}
		return null;
	}

	public boolean isWellFormed(){
		for(Integer id: bank.keySet()){
				if (persons.get(id)==null)
					return false;
				for (Account account: bank.get(id)){
					if (account == null)
						return false;
				}
		}
		return true;		
	}
	
	public void setBankMap(HashMap<Integer, Set<Account>> map){
		this.bank = map;
	}
	
	public Map<Integer, Set<Account>> getBankMap(){
		return this.bank;
	}
	
	public void setPersonMap(HashMap<Integer, Person> map){
		this.persons = map;
	}
	
	public Map<Integer, Person> getPersonMap(){
		return this.persons;
	}
	
	public void setIds(int ida, int idp){
		idA = ida;
		idP = idp;
	}
	
	public int getIdA(){
		return idA;
	}
	
	public int getIdP(){
		return idP;
	}
	
}
