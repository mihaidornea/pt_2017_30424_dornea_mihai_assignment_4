package tema4;

public class SavingAccount extends Account{

	private double interest = 1.5;
	private static final long serialVersionUID = -9070660477841639713L;

	public SavingAccount(int id, double sum) {
		super(id, sum);
	}

	@Override
	public void deposit(double sum) {
		double depositSum = sum + (sum*interest)/100;
		depositSum += getSum();
		setSum(depositSum);
	}
	
	@Override
	public void withdraw(double sum) {
		setSum(getSum() - sum);
	}
	
	public void setInteres(double interest){
		this.interest = interest;
	}
	
	public double getInteres(){
		return this.interest;
	}

}
