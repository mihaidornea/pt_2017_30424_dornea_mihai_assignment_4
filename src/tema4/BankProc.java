package tema4;

import java.util.List;
import java.util.Set;

public interface BankProc {
	public void createSavingAccount(Person person, double sum);
	public void createSpendingAccount(Person person, double sum);
	public void createPerson(String name, double sum);
	public void deposit(Person person, int accountId, double sum);
	public void withdraw(Person person, int accountId, double sum) throws Exception;
	public void deleteAccount(Person person, int accountId);
	public void deletePerson(Person person);
	public String checkBalance(Person person, int accountId);
	public Set<Account> listAccounts(Person person);
	public List<Person> listPersons();
}
