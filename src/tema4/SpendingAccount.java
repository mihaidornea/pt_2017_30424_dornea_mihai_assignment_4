package tema4;

public class SpendingAccount extends Account{
	
	private static final long serialVersionUID = -4003448565572707753L;

	public SpendingAccount(int id, double sum) {
		super(id, sum);
	}

	@Override
	public void deposit(double sum) {
		setSum(getSum() + sum);
	}

	@Override
	public void withdraw(double sum) {
		setSum(getSum() - sum);
	}

}
