package tema4;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import gui.MainPage;

public class Main {
	
	public static void main(String[] args){
		Bank bank = new Bank();
		MainPage view = new MainPage(bank);
		view.start(bank);
	}
}
