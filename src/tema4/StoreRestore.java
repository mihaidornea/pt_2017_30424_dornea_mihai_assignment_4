package tema4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Set;

public class StoreRestore {

	public static void save(Map<Integer, Set<Account>> accountMap, Map<Integer, Person> personMap, int idA, int idP){
		try {
			FileOutputStream fileStreamAccount = new FileOutputStream("saveAccountFile.data");
			FileOutputStream fileStreamPerson = new FileOutputStream("savePersonFile.data");
			FileOutputStream fileStreamIdA = new FileOutputStream("accountId.data");
			FileOutputStream fileStreamIdP = new FileOutputStream("personId.data");
			ObjectOutputStream objectStreamIdA = new ObjectOutputStream(fileStreamIdA);
			ObjectOutputStream objectStreamIdP = new ObjectOutputStream(fileStreamIdP);
			ObjectOutputStream objectStreamAccount = new ObjectOutputStream(fileStreamAccount);
			ObjectOutputStream objectStreamPerson = new ObjectOutputStream(fileStreamPerson);
			
			objectStreamIdP.writeInt(idP);
			objectStreamIdA.writeInt(idA);
			objectStreamAccount.writeObject(accountMap);
			objectStreamPerson.writeObject(personMap);
			
			objectStreamIdA.close();
			objectStreamIdP.close();
			objectStreamAccount.close();
			objectStreamPerson.close();
			fileStreamAccount.close();
			fileStreamPerson.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static Map<Integer, Set<Account>> loadAccount(){
		try {
			FileInputStream fileStream = new FileInputStream("saveAccountFile.data");
			ObjectInputStream objectStream = new ObjectInputStream(fileStream);
			Map<Integer, Set<Account>> map = (Map<Integer, Set<Account>>) objectStream.readObject();
			objectStream.close();
			fileStream.close();
			return map;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static Map<Integer, Person> loadPerson(){
		try {
			FileInputStream fileStream = new FileInputStream("savePersonFile.data");
			ObjectInputStream objectStream = new ObjectInputStream(fileStream);
			Map<Integer, Person> map = (Map<Integer, Person>) objectStream.readObject();
			objectStream.close();
			fileStream.close();
			return map;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static int loadIdA(){
		try {
			FileInputStream fileStream = new FileInputStream("accountId.data");
			ObjectInputStream objectStream = new ObjectInputStream(fileStream);
			int idA = objectStream.readInt();
			objectStream.close();
			fileStream.close();
			return idA;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public static int loadIdP(){
		try {
			FileInputStream fileStream = new FileInputStream("personId.data");
			ObjectInputStream objectStream = new ObjectInputStream(fileStream);
			int idP = objectStream.readInt();
			objectStream.close();
			fileStream.close();
			return idP;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}
	
}
