package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import tema4.Bank;
import tema4.Person;

public class DeleteAccount extends JFrame {

	private JPanel contentPane;
	private static DeleteAccount frame;
	private JTextField name;
	private JTextField personId;
	private JTextField accountId;
	/**
	 * Launch the application.
	 */
	public static void start(Bank bank) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new DeleteAccount(bank);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DeleteAccount(Bank bank) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 261, 215);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		name = new JTextField();
		name.setBounds(10, 33, 86, 20);
		contentPane.add(name);
		name.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 11, 46, 14);
		contentPane.add(lblName);
		
		personId = new JTextField();
		personId.setBounds(10, 85, 86, 20);
		contentPane.add(personId);
		personId.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Person Id");
		lblNewLabel.setBounds(10, 64, 46, 14);
		contentPane.add(lblNewLabel);
		
		accountId = new JTextField();
		accountId.setBounds(10, 137, 86, 20);
		contentPane.add(accountId);
		accountId.setColumns(10);
		
		JLabel lblAccountId = new JLabel("Account Id");
		lblAccountId.setBounds(10, 116, 62, 14);
		contentPane.add(lblAccountId);
		
		JButton btnDeleteAccount = new JButton("Delete Account");
		btnDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = getNameField().getText();
				int personId = Integer.parseInt(getPersonId().getText());
				int accountId = Integer.parseInt(getAccountId().getText());
				Person person = new Person(name, personId);
				bank.deleteAccount(person, accountId);
				setVisible(false);
			}
		});
		btnDeleteAccount.setBounds(126, 72, 105, 46);
		contentPane.add(btnDeleteAccount);
	}
	
	public JTextField getNameField(){
		return this.name;
	}
	
	public JTextField getPersonId(){
		return this.personId;
	}
	
	public JTextField getAccountId(){
		return this.accountId;
	}

}
