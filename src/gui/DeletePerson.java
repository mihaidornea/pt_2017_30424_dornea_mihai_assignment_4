package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import tema4.Bank;
import tema4.Person;

public class DeletePerson extends JFrame {

	private JPanel contentPane;
	private static DeletePerson frame;
	private JTextField name;
	private JTextField personId;
	/**
	 * Launch the application.
	 */
	public static void start(Bank bank) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new DeletePerson(bank);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DeletePerson(Bank bank) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 278, 199);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 22, 46, 14);
		contentPane.add(lblName);
		
		name = new JTextField();
		name.setBounds(10, 47, 86, 20);
		contentPane.add(name);
		name.setColumns(10);
		
		JLabel lblPersonId = new JLabel("Person Id");
		lblPersonId.setBounds(10, 78, 46, 14);
		contentPane.add(lblPersonId);
		
		personId = new JTextField();
		personId.setBounds(10, 103, 86, 20);
		contentPane.add(personId);
		personId.setColumns(10);
		
		JButton btnDeletePerson = new JButton("Delete Person");
		btnDeletePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = getNameField().getText();
				int personId = Integer.parseInt(getPersonId().getText());
				Person person = new Person(name, personId);
				bank.deletePerson(person);
				setVisible(false);
			}
		});
		btnDeletePerson.setBounds(119, 74, 106, 33);
		contentPane.add(btnDeletePerson);
	}

	public JTextField getNameField(){
		return this.name;
	}
	
	public JTextField getPersonId(){
		return this.personId;
	}
	
}
