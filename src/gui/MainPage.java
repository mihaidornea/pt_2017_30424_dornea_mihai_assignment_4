package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import tema4.Account;
import tema4.Bank;
import tema4.Person;
import tema4.StoreRestore;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class MainPage extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private Bank bank; 
	private JTextField changeName;
	/**
	 * Launch the application.
	 */
	public static void start(Bank bank) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainPage frame = new MainPage(bank);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainPage(Bank bank) {
		setTitle("Bank");
		this.bank = bank;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 857, 463);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Add Person");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddPerson view = new AddPerson(bank);
				view.start(bank);
			}
		});
		btnNewButton.setBounds(10, 11, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Add Account");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAccount view = new AddAccount(bank);
				view.start(bank);
			}
		});
		btnNewButton_1.setBounds(109, 11, 104, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnDeleteAccount = new JButton("Delete Account");
		btnDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DeleteAccount view = new DeleteAccount(bank);
				view.start(bank);
			}
		});
		btnDeleteAccount.setBounds(223, 11, 119, 23);
		contentPane.add(btnDeleteAccount);
		
		JButton btnWithdrawMoney = new JButton("Withdraw money");
		btnWithdrawMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Withdraw view = new Withdraw(bank);
				view.start(bank);
			}
		});
		btnWithdrawMoney.setBounds(357, 11, 113, 23);
		contentPane.add(btnWithdrawMoney);
		
		JButton btnNewButton_2 = new JButton("Deposit");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Deposit view = new Deposit(bank);
				view.start(bank);
			}
		});
		btnNewButton_2.setBounds(480, 11, 104, 23);
		contentPane.add(btnNewButton_2);
		
		JButton btnDeletePerson = new JButton("Delete Person");
		btnDeletePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DeletePerson view = new DeletePerson(bank);
				view.start(bank);
			}
		});
		btnDeletePerson.setBounds(594, 11, 113, 23);
		contentPane.add(btnDeletePerson);
		
		JButton btnListPersons = new JButton("List Persons");
		btnListPersons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listPersons();
			}
		});
		btnListPersons.setBounds(717, 11, 89, 23);
		contentPane.add(btnListPersons);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = table.rowAtPoint(arg0.getPoint());
				int col = table.columnAtPoint(arg0.getPoint());
				if (row >= 1 && col == 1){
					table.getModel().setValueAt(getChangeName().getText(), row, col);
					int id = (int) table.getModel().getValueAt(row, 0);
					Map<Integer, Person> persons = bank.getPersonMap();
					Person person = persons.get(id);
					person.setName(getChangeName().getText());
				}
			}
		});
		table.setBounds(10, 60, 821, 353);
		contentPane.add(table);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StoreRestore saver = new StoreRestore();
				saver.save(bank.getBankMap(), bank.getPersonMap(), bank.getIdA(), bank.getIdP());
			}
		});
		btnSave.setBounds(10, 34, 89, 23);
		contentPane.add(btnSave);
		
		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StoreRestore loader = new StoreRestore();
				bank.setBankMap((HashMap<Integer, Set<Account>>) loader.loadAccount());
				bank.setPersonMap((HashMap<Integer, Person>) loader.loadPerson());
				bank.setIds(loader.loadIdA(), loader.loadIdP());
			}
		});
		btnLoad.setBounds(109, 34, 89, 23);
		contentPane.add(btnLoad);
		
		changeName = new JTextField();
		changeName.setBounds(352, 35, 138, 20);
		contentPane.add(changeName);
		changeName.setColumns(10);
		
		JLabel lblChangeNameIn = new JLabel("Change Name In Table");
		lblChangeNameIn.setBounds(208, 38, 134, 14);
		contentPane.add(lblChangeNameIn);
	}
	
	public void listPersons(){
		DefaultTableModel model = new DefaultTableModel();
		Object[] columns = new Object[4];
		Map<Integer, Person> personMap = bank.getPersonMap();
		Map<Integer, Set<Account>> accountMap = bank.getBankMap();
		
			columns[0] = "Person ID";
			columns[1] = "Person Name";
			columns[2] = "Account ID";
			columns[3] = "Sum Deposited";
		
		model.setColumnIdentifiers(columns);
		model.addRow(columns);
		
		Object[] rowData = new Object[4];
		
		for(Integer id: accountMap.keySet()){
			for (Account account: accountMap.get(id)){
				rowData[0] = personMap.get(id).getId();
				rowData[1] = personMap.get(id).getName();
				rowData[2] = account.getId();
				rowData[3] = account.getSum();
				model.addRow(rowData);
			}
		}
		table.setModel(model);
	}
	
	public JTextField getChangeName(){
		return this.changeName;
	}
}
