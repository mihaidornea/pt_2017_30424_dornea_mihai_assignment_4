package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import tema4.Bank;
import tema4.Person;

public class AddAccount extends JFrame {

	private JPanel contentPane;
	private static AddAccount frame;
	private JTextField personId;
	private JTextField personName;
	private JTextField depositSum;
	private JCheckBox check;
	/**
	 * Launch the application.
	 */
	public static void start(Bank bank) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AddAccount(bank);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddAccount(Bank bank) {
		setTitle("AddAccount");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 280, 248);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		personId = new JTextField();
		personId.setBounds(10, 32, 86, 20);
		contentPane.add(personId);
		personId.setColumns(10);
		
		JLabel lblId = new JLabel("Person Id");
		lblId.setBounds(10, 11, 46, 14);
		contentPane.add(lblId);
		
		personName = new JTextField();
		personName.setBounds(10, 84, 86, 20);
		contentPane.add(personName);
		personName.setColumns(10);
		
		JLabel lblPersonName = new JLabel("Person Name");
		lblPersonName.setBounds(10, 59, 86, 14);
		contentPane.add(lblPersonName);
		
		depositSum = new JTextField();
		depositSum.setBounds(10, 133, 86, 20);
		contentPane.add(depositSum);
		depositSum.setColumns(10);
		
		JLabel lblDepositSum = new JLabel("Deposit Sum");
		lblDepositSum.setBounds(10, 115, 67, 14);
		contentPane.add(lblDepositSum);
		
		check = new JCheckBox("Spending/Saving");
		check.setBounds(10, 174, 105, 23);
		contentPane.add(check);
		
		JButton btnCreateAccount = new JButton("Create Account");
		btnCreateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = getNameField().getText();
				int id = Integer.parseInt(getIdField().getText());
				double sum = Double.parseDouble(getSumField().getText());
				Person person = new Person(name, id);
				
				if (getCheck().isSelected()){ 
					bank.createSavingAccount(person, sum);
				}
				else {
					bank.createSpendingAccount(person, sum);
				}
				
				setVisible(false);
			}
		});
		btnCreateAccount.setBounds(132, 83, 115, 46);
		contentPane.add(btnCreateAccount);
	}
	
	public JTextField getNameField(){
		return this.personName;
	}
	
	public JTextField getSumField(){
		return this.depositSum;
	}
	
	public JTextField getIdField(){
		return this.personId;
	}
	
	public JCheckBox getCheck(){
		return this.check;
	}
	
	
}
