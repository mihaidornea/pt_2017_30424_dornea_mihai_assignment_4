package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import tema4.Bank;

public class AddPerson extends JFrame {
	private static AddPerson frame;
	private JPanel contentPane;
	private JTextField name;
	private JTextField sum;
	private JLabel lblSum;
	private JButton btnCreatePerson;

	/**
	 * Launch the application.
	 */
	public static void start (Bank bank) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new AddPerson(bank);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddPerson(Bank bank) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 241, 185);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		name = new JTextField();
		name.setBounds(10, 31, 86, 20);
		contentPane.add(name);
		name.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(20, 6, 46, 14);
		contentPane.add(lblName);
		
		sum = new JTextField();
		sum.setBounds(10, 84, 86, 20);
		contentPane.add(sum);
		sum.setColumns(10);
		
		lblSum = new JLabel("Sum");
		lblSum.setBounds(20, 62, 46, 14);
		contentPane.add(lblSum);
		
		btnCreatePerson = new JButton("Create Person");
		btnCreatePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = getNameField().getText();
				double sum = Double.parseDouble(getSumField().getText());
				bank.createPerson(name, sum);
				setVisible(false);
			}
		});
		btnCreatePerson.setBounds(114, 43, 101, 52);
		contentPane.add(btnCreatePerson);
	}
	
	public JTextField getNameField(){
		return this.name;
	}

	public JTextField getSumField(){
		return this.sum;
	}
	
}
