package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import tema4.Bank;
import tema4.Person;

public class Withdraw extends JFrame {

	private JPanel contentPane;
	private static Withdraw frame;
	private JTextField name;
	private JTextField personId;
	private JTextField accountId;
	private JTextField sum;
	/**
	 * Launch the application.
	 */
	public static void start(Bank bank) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Withdraw(bank);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Withdraw(Bank bank) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 285, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 11, 46, 14);
		contentPane.add(lblName);
		
		name = new JTextField();
		name.setBounds(10, 36, 86, 20);
		contentPane.add(name);
		name.setColumns(10);
		
		JLabel lblPersonId = new JLabel("Person Id");
		lblPersonId.setBounds(10, 67, 62, 14);
		contentPane.add(lblPersonId);
		
		personId = new JTextField();
		personId.setBounds(10, 95, 86, 20);
		contentPane.add(personId);
		personId.setColumns(10);
		
		JLabel lblAccountId = new JLabel("Account Id");
		lblAccountId.setBounds(10, 126, 62, 14);
		contentPane.add(lblAccountId);
		
		accountId = new JTextField();
		accountId.setBounds(10, 151, 86, 20);
		contentPane.add(accountId);
		accountId.setColumns(10);
		
		JLabel lblWithdrawSum = new JLabel("Withdraw Sum");
		lblWithdrawSum.setBounds(10, 182, 86, 14);
		contentPane.add(lblWithdrawSum);
		
		sum = new JTextField();
		sum.setBounds(10, 207, 86, 20);
		contentPane.add(sum);
		sum.setColumns(10);
		
		JButton btnNewButton = new JButton("Withdraw");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = getNameField().getText();
				int personId = Integer.parseInt(getPersonId().getText());
				int accountId = Integer.parseInt(getAccountId().getText());
				double sum = Double.parseDouble(getSum().getText());
				Person person = new Person(name, personId);
				try {
					bank.withdraw(person, accountId, sum);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				setVisible(false);
			}
		});
		btnNewButton.setBounds(122, 95, 135, 76);
		contentPane.add(btnNewButton);
	}

	public JTextField getNameField(){
		return this.name;
	}
	
	public JTextField getPersonId(){
		return this.personId;
	}
	
	public JTextField getAccountId(){
		return this.accountId;
	}
	
	public JTextField getSum(){
		return this.sum;
	}
	
}
